/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// linestate.go: State machine for reading unified diffs

package rdudiff

import (
	"fmt"
	"io"
	"strings"
)

type linestate int

const (
	stateLookForFileHeader linestate = iota
	stateFileHeader2
	stateLookForHunkOrFileHeader
	stateHunk
	stateFormatError
)

type formaterror struct {
	internalErr error
}

func (fe *formaterror) Error() string {
	if fe.internalErr == nil {
		return "Unified diff format error"
	}
	return fmt.Sprintf("Unified diff format error(%v)", fe.internalErr)
}

func (fe *formaterror) Unwrap() error {
	return fe.internalErr
}

// FormatError is returned when the input diff file doesn't match the unified diff format.
var FormatError *formaterror = &formaterror{}

func (dr *Reader) stepState() (string, error) {
	ret := ""
readloop:
	for {
		dr.justFoundFile = false
		dr.justFoundHunk = false
		dr.justFoundLineDel = false
		dr.justFoundLineAdd = false
		dr.justFoundLineContext = false

		line, err := dr.r.ReadString('\n') // returns io.EOF at end of file
		if err == io.EOF && line != "" {   // handle missing newline at end of file
			err = nil
		}
		dr.linenum++
		if err != nil {
			return ret, fmt.Errorf("%w, when parsing diff on line %d", err, dr.linenum)
		}
		lineraw := line
		line = strings.TrimRight(line, "\r\n") // get rid of those pesky line endings

		switch dr.state {
		case stateLookForFileHeader:
			if strings.HasPrefix(line, "--- ") {
				var t string
				var err error
				dr.stateFile.OrigPath, t, err = splitName(line[4:])
				if err != nil {
					dr.state = stateFormatError
					return ret, fmt.Errorf("%w: line %d", &formaterror{internalErr: err}, dr.linenum)
				}
				dr.stateFile.NewTimestamp = t
				dr.state = stateFileHeader2
				continue readloop
			}
		case stateFileHeader2:
			if strings.HasPrefix(line, "+++ ") {
				var t string
				var err error
				dr.stateFile.NewPath, t, err = splitName(line[4:])
				if err != nil {
					dr.state = stateFormatError
					return ret, fmt.Errorf("%w: line %d", &formaterror{internalErr: err}, dr.linenum)
				}
				dr.stateFile.OrigTimestamp = t
				dr.state = stateLookForHunkOrFileHeader
				dr.justFoundFile = true
				break readloop
			} else {
				dr.state = stateFormatError
				return ret, fmt.Errorf("%w: line %d", FormatError, dr.linenum)
			}
		case stateLookForHunkOrFileHeader:
			if strings.HasPrefix(line, "--- ") {
				var t string
				var err error
				dr.stateFile.OrigPath, t, err = splitName(line[4:])
				if err != nil {
					dr.state = stateFormatError
					return ret, fmt.Errorf("%w: line %d", &formaterror{internalErr: err}, dr.linenum)
				}
				dr.stateFile.NewTimestamp = t
				dr.state = stateFileHeader2
				continue readloop
			}
			if strings.HasPrefix(line, "@@ -") {
				n, err := fmt.Sscanf(line, "@@ -%d,%d +%d,%d @@", &dr.stateHunk.OrigRange.StartLine, &dr.stateHunk.OrigRange.NumLines, &dr.stateHunk.NewRange.StartLine, &dr.stateHunk.NewRange.NumLines)
				if err != nil {
					dr.state = stateFormatError
					return ret, fmt.Errorf("%w: line %d", &formaterror{internalErr: err}, dr.linenum)
				}
				if n < 4 {
					dr.state = stateFormatError
					return ret, fmt.Errorf("%w: line %d", FormatError, dr.linenum)
				}
				sec := strings.Split(line, "@@")
				dr.stateHunk.SectionHeading = strings.TrimLeft(strings.Join(sec[2:], "@@"), " ")
				dr.state = stateHunk
				dr.justFoundHunk = true
				break readloop
			}
			dr.state = stateLookForFileHeader
			continue readloop
		case stateHunk:
			line = lineraw
			if len(line) > 1 {
				ret = line[1:]
			} else {
				dr.state = stateFormatError
				return ret, fmt.Errorf("%w: line %d", FormatError, dr.linenum)
			}
			if strings.HasPrefix(line, "+") {
				dr.justFoundLineAdd = true
				dr.stateHunk.NewRange.NumLines--
			} else if strings.HasPrefix(line, "-") {
				dr.justFoundLineDel = true
				dr.stateHunk.OrigRange.NumLines--
			} else if strings.HasPrefix(line, " ") {
				dr.justFoundLineContext = true
				dr.stateHunk.NewRange.NumLines--
				dr.stateHunk.OrigRange.NumLines--
			} else {
				dr.state = stateFormatError
				return ret, fmt.Errorf("%w: line %d", FormatError, dr.linenum)
			}
			if dr.stateHunk.NewRange.NumLines < 0 || dr.stateHunk.OrigRange.NumLines < 0 {
				dr.state = stateFormatError
				return ret, fmt.Errorf("%w: line %d", FormatError, dr.linenum)
			}
			if dr.stateHunk.NewRange.NumLines == 0 || dr.stateHunk.OrigRange.NumLines == 0 {
				dr.state = stateLookForHunkOrFileHeader
			}
			break readloop
		case stateFormatError:
			return ret, fmt.Errorf("%w: line %d", FormatError, dr.linenum)
		}
	}
	return ret, nil
}

func splitName(filetime string) (name string, time string, err error) {
	str := strings.Split(filetime, "\t")
	if len(str) < 2 {
		return "", "", fmt.Errorf("missing tab")
	}
	return str[0], strings.Join(str[1:], "\t"), nil
}
