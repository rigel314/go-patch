/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// rdudiff.go: Unified diff reader implementation

package rdudiff

import (
	"bufio"
	"io"
)

// A Reader parses a unified diff.
type Reader struct {
	r                    *bufio.Reader
	linenum              int
	state                linestate
	stateFile            File
	stateHunk            Hunk
	justFoundFile        bool
	justFoundHunk        bool
	justFoundLineContext bool
	justFoundLineAdd     bool
	justFoundLineDel     bool
}

// A File contains the metadata for the diff of a particular file.
type File struct {
	OrigPath      string
	NewPath       string
	OrigTimestamp string
	NewTimestamp  string
}

// A Hunk contains the metadata for a particular diffed section of the previous file.
type Hunk struct {
	OrigRange      Range
	NewRange       Range
	SectionHeading string
}

// A Range holds the information in a unified diff range specification.
type Range struct {
	StartLine int
	NumLines  int
}

// Type is the kind of diff line inside a hunk.
type Type int

const (
	// Deletion is the Type for a deleted line, when performing a patch, should verify that this exists, then omit it
	Deletion Type = iota - 1
	// Context is the Type for a line of context, when performing a patch, should verify that this context exists
	Context
	// Addition is the Type for an added line, when performing a patch, should include this
	Addition
)

// NewReader creates a new reader, reading from r.
func NewReader(r io.Reader) *Reader {
	dr := new(Reader)
	dr.r = bufio.NewReader(r)
	return dr
}

// NextFile advances to the next file in the diff, including the first.  Any remaining hunks in the current file are automatically discarded.
//
// io.EOF is returned at the end of the diff.
func (dr *Reader) NextFile() (*File, error) {
	for !dr.justFoundFile {
		_, err := dr.stepState()
		if err != nil {
			return nil, err
		}
	}
	dr.justFoundFile = false
	var f File = dr.stateFile
	return &f, nil
}

// NextHunk advances to the next hunk in the current file, including the first.  Any remaining lines in the current hunk are automatically discarded.
//
// io.EOF is returned at the end of the file.
func (dr *Reader) NextHunk() (*Hunk, error) {
	for !dr.justFoundHunk && !dr.justFoundFile {
		_, err := dr.stepState()
		if err != nil {
			return nil, err
		}
	}
	if dr.justFoundFile {
		return nil, io.EOF
	}
	dr.justFoundHunk = false
	var h Hunk = dr.stateHunk
	return &h, nil
}

// ReadLine reads one line from the current hunk.  Returns the actual file content, and a Type indicating what to do with the line.
//
// io.EOF is returned at the end of the hunk.
func (dr *Reader) ReadLine() (line []byte, t Type, err error) {
	var l string
	for !dr.justFoundLineAdd && !dr.justFoundLineContext && !dr.justFoundLineDel && !dr.justFoundHunk && !dr.justFoundFile {
		l, err = dr.stepState()
		if err != nil {
			return nil, Context, err
		}
	}
	if dr.justFoundFile || dr.justFoundHunk {
		return nil, Context, io.EOF
	}
	if dr.justFoundLineAdd {
		dr.justFoundLineAdd = false
		return []byte(l), Addition, nil
	}
	if dr.justFoundLineDel {
		dr.justFoundLineDel = false
		return []byte(l), Deletion, nil
	}
	if dr.justFoundLineContext {
		dr.justFoundLineContext = false
		return []byte(l), Context, nil
	}
	return nil, Context, io.EOF // shouldn't be reachable
}
