/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// testsRead_test.go: Tests for reading unified diff files

package rdudiff

import (
	"errors"
	"io"
	"strings"
	"testing"
)

func TestFileRead(t *testing.T) {
	var sreader = strings.NewReader(nominal)
	var dreader = NewReader(sreader)

	var err error
	var idx int
	var f *File
	for {
		f, err = dreader.NextFile()
		if err != nil && !errors.Is(err, io.EOF) {
			t.Log("Wrong error:", err)
			t.Fail()
			break
		}
		if errors.Is(err, io.EOF) {
			break
		}
		t.Log(f)
		idx++
	}
	if idx != 4 {
		t.Log("Invalid itterations:", idx)
		t.Fail()
	}
}

func TestHunkRead(t *testing.T) {
	var sreader = strings.NewReader(nominal)
	var dreader = NewReader(sreader)

	var err error
	var f *File
	var h *Hunk
	f, err = dreader.NextFile()
	if err != nil {
		t.Log("NextFile error:", err)
		t.FailNow()
	}
	t.Log("file:", f)

	h, err = dreader.NextHunk()
	if err != nil {
		t.Log("NextHunk error:", err)
		t.FailNow()
	}
	t.Log("hunk:", h)

	for {
		l, typ, err := dreader.ReadLine()
		if err != nil && !errors.Is(err, io.EOF) {
			t.Log("Wrong error:", err)
			t.Fail()
			break
		}
		if errors.Is(err, io.EOF) {
			break
		}
		t.Log(typ, l)
	}
}

func TestFirstFile(t *testing.T) {
	var sreader = strings.NewReader(nominal)
	var dreader = NewReader(sreader)

	f, err := dreader.NextFile()
	if err != nil {
		t.Log("NextFile error:", err)
		t.Fail()
	}
	t.Log("fail:", f)
	if f.OrigPath != "foo/bar1" {
		t.Log("Bad OrigPath")
		t.Fail()
	}
	if f.NewPath != "foo/bar1" {
		t.Log("Bad NewPath")
		t.Fail()
	}
}

func TestFirstHunk(t *testing.T) {
	var sreader = strings.NewReader(nominal)
	var dreader = NewReader(sreader)

	f, err := dreader.NextFile()
	if err != nil {
		t.Log("NextFile error:", err)
		t.Fail()
	}
	t.Log("file:", f)
	h, err := dreader.NextHunk()
	if err != nil {
		t.Log("NextHunk error:", err)
		t.FailNow()
	}
	t.Log("hunk:", h)
	if h.NewRange.StartLine != 1 || h.NewRange.NumLines != 3 {
		t.Log("Bad StartLine")
		t.Fail()
	}
	if h.OrigRange.StartLine != 1 || h.OrigRange.NumLines != 3 {
		t.Log("Bad StartLine")
		t.Fail()
	}
	if h.SectionHeading != "section header" {
		t.Log("Bad SectionHeading")
		t.Fail()
	}
}

func TestReadTooMany(t *testing.T) {
	var sreader = strings.NewReader(nominal)
	var dreader = NewReader(sreader)

	f, err := dreader.NextFile()
	if err != nil {
		t.Log("NextFile error:", err)
		t.Fail()
	}
	t.Log("file:", f)
	h, err := dreader.NextHunk()
	if err != nil {
		t.Log("NextHunk error:", err)
		t.FailNow()
	}
	t.Log("hunk:", h)
	for {
		_, _, err := dreader.ReadLine()
		if err != nil && !errors.Is(err, io.EOF) {
			t.Log("Wrong error:", err)
			t.Fail()
			break
		}
		if errors.Is(err, io.EOF) {
			break
		}
	}
	_, _, err = dreader.ReadLine()
	if !errors.Is(err, io.EOF) {
		t.Log("Wrong error:", err)
		t.Fail()
	}
	for {
		_, err := dreader.NextHunk()
		if err != nil && !errors.Is(err, io.EOF) {
			t.Log("Wrong error:", err)
			t.Fail()
			break
		}
		if errors.Is(err, io.EOF) {
			break
		}
	}
	_, err = dreader.NextHunk()
	if !errors.Is(err, io.EOF) {
		t.Log("Wrong error:", err)
		t.Fail()
	}
	for {
		_, err := dreader.NextFile()
		if err != nil && !errors.Is(err, io.EOF) {
			t.Log("Wrong error:", err)
			t.Fail()
			break
		}
		if errors.Is(err, io.EOF) {
			break
		}
	}
	_, err = dreader.NextFile()
	if !errors.Is(err, io.EOF) {
		t.Log("Wrong error:", err)
		t.Fail()
	}
}

func TestMissingFileRead(t *testing.T) {
	var sreader = strings.NewReader(missingfiles)
	var dreader = NewReader(sreader)

	var err error
	var idx int
	var f *File
	for {
		f, err = dreader.NextFile()
		if err != nil && !errors.Is(err, io.EOF) {
			t.Log("Wrong error:", err)
			t.Fail()
			break
		}
		if errors.Is(err, io.EOF) {
			break
		}
		t.Log(f)
		idx++
	}
	if idx != 4 {
		t.Log("Invalid itterations:", idx)
		t.Fail()
	}
}

var nominal = `
--- foo/bar1	timestamp
+++ foo/bar1	timestamp
@@ -1,3 +1,3 @@ section header
 foo
-bar
+baz
 bat
--- foo/bar2	timestamp
+++ foo/bar2	timestamp
@@ -1,3 +1,3 @@
 foo
-bar
+baz
 bat
--- foo/bar3	timestamp
+++ foo/bar3	timestamp
@@ -1,3 +1,3 @@
 foo
-bar
+baz
 bat
--- foo/bar4	timestamp
+++ foo/bar4	timestamp
@@ -1,3 +1,3 @@
 foo
-bar
+baz
 bat
`

var missingfiles = `
No such file foo/bar
--- foo/bar1	timestamp
+++ foo/bar1	timestamp
@@ -1,3 +1,3 @@ section header
 foo
-bar
+baz
 bat
--- foo/bar2	timestamp
+++ foo/bar2	timestamp
@@ -1,3 +1,3 @@
 foo
-bar
+baz
 bat
No such file foo/baz
--- foo/bar3	timestamp
+++ foo/bar3	timestamp
@@ -1,3 +1,3 @@
 foo
-bar
+baz
 bat
--- foo/bar4	timestamp
+++ foo/bar4	timestamp
@@ -1,3 +1,3 @@
 foo
-bar
+baz
 bat
`
