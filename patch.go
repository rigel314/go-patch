/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// patch.go: Main implementation of the patch function

package patch

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/rigel314/go-patch/rdudiff"
)

// Patch will perform a text file patch operation from a unified diff
func Patch(dest string, udiff io.Reader, strip int, reverse bool) error {
	// df, err := os.Open(udiff)
	// if err != nil {
	// 	return err
	// }
	// defer df.Close()

	rdu := rdudiff.NewReader(udiff)

	info, err := os.Stat(dest)
	if err != nil {
		return err
	}
	outf, err := ioutil.TempFile("", "outf*.tmp")
	if err != nil {
		return err
	}
	defer os.Remove(outf.Name())

	outwr := bufio.NewWriter(outf)
	for { // Each file mentioned in the diff
		f, err := rdu.NextFile()
		if err != nil && !errors.Is(err, io.EOF) {
			return err
		}
		if errors.Is(err, io.EOF) {
			break
		}
		if !info.IsDir() {
			f.NewPath = dest
		} else {
			paths := pathSplit(f.NewPath)
			if len(paths) > strip {
				paths = paths[strip:]
			}
			f.NewPath = filepath.Join(dest, filepath.Join(paths...))
		}
		infile := f.NewPath
		if infile != "/dev/null" {
			// TODO: create folder path
			inf, err := os.OpenFile(infile, os.O_RDWR|os.O_CREATE, 0755)
			if err != nil {
				return err
			}
			inrd := bufio.NewReader(inf)

			_, err = outf.Seek(0, os.SEEK_SET)
			if err != nil {
				return err
			}
			err = outf.Truncate(0)
			if err != nil {
				return err
			}
			outwr.Reset(outf)
			sl := 0
			for { // Each hunk in the file in the diff
				h, err := rdu.NextHunk()
				if err != nil && !errors.Is(err, io.EOF) {
					return err
				}
				if errors.Is(err, io.EOF) {
					break
				}
				if reverse {
					t := h.NewRange
					h.NewRange = h.OrigRange
					h.OrigRange = t
				}
				// Copy lines upto the indicated orig line number of the hunk
				for ; sl < h.OrigRange.StartLine-1; sl++ {
					b, err := inrd.ReadBytes('\n')
					if err != nil {
						return err
					}
					_, err = outwr.Write(b)
					if err != nil {
						return err
					}
				}
				for { // Each line in the hunk
					l, t, err := rdu.ReadLine()
					if err != nil && !errors.Is(err, io.EOF) {
						return err
					}
					if errors.Is(err, io.EOF) {
						break
					}
					if reverse {
						switch t {
						case rdudiff.Addition:
							t = rdudiff.Deletion
						case rdudiff.Deletion:
							t = rdudiff.Addition
						}
					}
					switch t {
					case rdudiff.Addition:
						_, err := outwr.Write(l)
						if err != nil {
							return err
						}
					case rdudiff.Deletion:
						// verify only
						b, err := inrd.ReadBytes('\n')
						if err != nil && !errors.Is(err, io.EOF) {
							return err
						}
						if bytes.Compare(b, l) != 0 {
							return newDestDiffMismatch(infile)
						}
						if err != nil {
							return err
						}
						sl++
					case rdudiff.Context:
						// verify, erroring if it fais, then write
						b, err := inrd.ReadBytes('\n')
						if err != nil && !errors.Is(err, io.EOF) {
							return err
						}
						if bytes.Compare(b, l) != 0 {
							return newDestDiffMismatch(infile)
						}
						_, werr := outwr.Write(b)
						if werr != nil {
							return werr
						}
						if err != nil {
							return err
						}
						sl++
					}
				}
				_, err = io.Copy(outwr, inrd)
				if err != nil {
					return err
				}
			}
			err = outwr.Flush()
			if err != nil {
				return err
			}
			_, err = inf.Seek(0, os.SEEK_SET)
			if err != nil {
				return err
			}
			err = inf.Truncate(0)
			if err != nil {
				return err
			}
			_, err = outf.Seek(0, os.SEEK_SET)
			if err != nil {
				return err
			}
			_, err = io.Copy(inf, outf)
			if err != nil {
				return err
			}
			inf.Close()
		} else {
			// TODO: delete file
		}

		if !info.IsDir() { // stop after first file
			break
		}
	}

	return nil
}

func pathSplit(path string) []string {
	paths := make([]string, 0, 10)
	for {
		base := filepath.Base(path)
		dir := filepath.Dir(path)
		if base == dir {
			break
		}
		path = dir
		paths = append(paths, base)
	}
	for left, right := 0, len(paths)-1; left < right; left, right = left+1, right-1 {
		paths[left], paths[right] = paths[right], paths[left]
	}
	return paths
}
