/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// go-patch.go: cli utility to patch files based on a unified diff

package main

import (
	"flag"
	"log"
	"os"
)

func main() {
	flag.Parse()

	path := flag.Arg(0)
	udiff, err := os.Open(path)
	if err != nil {
		log.Fatal("Failed to open unified diff file, ", udiff)
	}
	defer udiff.Close()

}
